<?php
	$dbHost = "localhost";
	$dbDatabase = "test";
	$dbPasswrod = "";
	$dbUser = "root";
	$mysqli = new mysqli($dbHost, $dbUser, $dbPasswrod, $dbDatabase);

    require('spreadsheet-reader/php-excel-reader/excel_reader2.php');
    require('spreadsheet-reader/SpreadsheetReader.php');

    if($_FILES['file_excel']['size'] != 0) {
        $mimes = [
            'application/vnd.ms-excel',
            'text/xls',
            'text/xlsx',
            'application/vnd.oasis.opendocument.spreadsheet', 
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ];
        
        if(in_array($_FILES["file_excel"]["type"], $mimes)) {
            $uploadFilePath = 'uploads/'.basename($_FILES['file_excel']['name']);
            move_uploaded_file($_FILES['file_excel']['tmp_name'], $uploadFilePath);

            $Reader = new SpreadsheetReader($uploadFilePath);
            $totalSheet = count($Reader->sheets());
            /* For Loop for all sheets */
            for($i = 0; $i < $totalSheet; $i++){
                $Reader->ChangeSheet($i);
                foreach ($Reader as $key => $row)
                {
                    if ($row[0] != 'Nhãn (label)') {
                        $label = isset($row[0]) ? $row[0] : null;
                        $dvt = isset($row[1]) ? $row[1] : null;
                        $qty = isset($row[2]) ? $row[2] : null;
                        $mrptype = isset($row[3]) ? $row[3] : 1;
                        $fk_bom = isset($row[4]) ? $row[4] : null;
                        $date_start_plan = isset($row[5]) ? $row[5] : null;
                        $date_end_plan = isset($row[6]) ? $row[6] : null;
                        $status = isset($row[7]) ? $row[7] : 1;
                        $tms = isset($row[8]) ? $row[8] : null;
                        $entity = 1;
                        $fk_user_create = 1;

                        $query = "insert into test(label, dvt, qty, mrptype, fk_bom, date_start_plan, date_end_plan, status, tms, entity, fk_user_create) 
                        values('".$label."', '".$dvt."', '".$qty."', '".$mrptype."', '".$fk_bom."', '".$date_start_plan."', '".$date_end_plan."', '".$status."', '".$tms."', '".$entity."', '".$fk_user_create."')";
                        $mysqli->query($query);
                        print_r($mysqli->query($query));
                    }
                }
            }
        } else {
            die("<br/>Sorry, File type is not allowed. Only Excel file.");
        }
    } else {
        die("Please upload file!");
    }
?>